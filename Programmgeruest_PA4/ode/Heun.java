package ode;

/**
 * Das Einschrittverfahren von Heun
 *
 * @author braeckle
 *
 */
public class Heun implements Einschrittverfahren {

    @Override
    /**
     * {@inheritDoc}
     * Nutzen Sie dabei geschickt den Expliziten Euler.
     */
    public double[] nextStep(double[] y_k, double t, double delta_t, ODE ode) {
        double[] a = ode.auswerten(t + delta_t, new ExpliziterEuler().nextStep(y_k, t, delta_t, ode));
        double[] b = ode.auswerten(t, y_k);

        double[] y = new double[y_k.length];

        for (int i = 0; i < y.length; i++) {
            y[i] = y_k[i] + (delta_t / 2) * (a[i] + b[i]);
        }

        return y;
    }

}

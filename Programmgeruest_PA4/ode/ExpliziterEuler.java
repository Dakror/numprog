package ode;

/**
 * Das Einschrittverfahren "Expliziter Euler"
 *
 * @author braeckle
 *
 */
public class ExpliziterEuler implements Einschrittverfahren {

    @Override
    public double[] nextStep(double[] y_k, double t, double delta_t, ODE ode) {
        double[] y = ode.auswerten(t, y_k);

        for (int i = 0; i < y.length; i++) {
            y[i] = y[i] * delta_t + y_k[i];
        }
        return y;
    }

}

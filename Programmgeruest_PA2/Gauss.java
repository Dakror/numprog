
public class Gauss {

    final static double eps = 1e-10;

    /**
     * Diese Methode soll die Loesung x des LGS R*x=b durch
     * Rueckwaertssubstitution ermitteln.
     * PARAMETER: 
     * R: Eine obere Dreiecksmatrix der Groesse n x n 
     * b: Ein Vektor der Laenge n
     */
    public static double[] backSubst(double[][] R, double[] b) {
        double[] x = new double[b.length];

        for (int i = R.length - 1; i >= 0; i--) {
            double s = 0;
            for (int j = i + 1; j < R.length; j++) {
                s += R[i][j] * x[j];
            }
            x[i] = (b[i] - s) / R[i][i];
        }

        return x;
    }

    /**
     * Diese Methode soll die Loesung x des LGS A*x=b durch Gauss-Elimination mit
     * Spaltenpivotisierung ermitteln. A und b sollen dabei nicht veraendert werden. 
     * PARAMETER: A:
     * Eine regulaere Matrix der Groesse n x n 
     * b: Ein Vektor der Laenge n
     */
    public static double[] solve(double[][] A, double[] b) {
        int n = A.length;

        // create working copy of matrix, including solution column
        double[][] R = new double[n][n + 1];
        for (int i = 0; i < n; i++) {
            System.arraycopy(A[i], 0, R[i], 0, n);
            R[i][n] = b[i];
        }

        for (int i = 0; i < n; i++) {
            // sort rows, highest at topmost position
            double max = Math.abs(R[i][i]);
            int j = i;

            // find max
            for (int k = i + 1; k < n; k++) {
                if (Math.abs(R[k][i]) > max) {
                    max = Math.abs(R[k][i]);
                    j = k;
                }
            }

            // i think you could even abort here, rang(A) != n ? not sure though.
            if (max == 0) continue;

            // swap rows
            if (j != i) {
                double[] t = R[i];
                R[i] = R[j];
                R[j] = t;
            }

            // subtract from lower rows
            for (int k = i + 1; k < n; k++) {
                double fac = R[k][i] / R[i][i];

                // subtract from every cell of the row
                for (int l = i; l < n + 1; l++) {
                    R[k][l] -= fac * R[i][l];
                }
            }
        }

        // extract nxn matrix and n vector from gauss matrix
        double[][] R1 = new double[n][n];
        double[] b1 = new double[n];

        for (int i = 0; i < n; i++) {
            System.arraycopy(R[i], 0, R1[i], 0, n);
            b1[i] = R[i][n];
        }

        // solve LGS
        return backSubst(R1, b1);
    }

    /**
     * Diese Methode soll eine Loesung p!=0 des LGS A*p=0 ermitteln. A ist dabei
     * eine nicht invertierbare Matrix. A soll dabei nicht veraendert werden.
     * 
     * Gehen Sie dazu folgendermassen vor (vgl.Aufgabenblatt): 
     * -Fuehren Sie zunaechst den Gauss-Algorithmus mit Spaltenpivotisierung 
     *  solange durch, bis in einem Schritt alle moeglichen Pivotelemente
     *  numerisch gleich 0 sind (d.h. <1E-10) 
     * -Betrachten Sie die bis jetzt entstandene obere Dreiecksmatrix T und
     *  loesen Sie Tx = -v durch Rueckwaertssubstitution 
     * -Geben Sie den Vektor (x,1,0,...,0) zurueck
     * 
     * Sollte A doch invertierbar sein, kann immer ein Pivot-Element gefunden werden(>=1E-10).
     * In diesem Fall soll der 0-Vektor zurueckgegeben werden. 
     * PARAMETER: 
     * A: Eine singulaere Matrix der Groesse n x n 
     */
    public static double[] solveSing(double[][] A) {
        int n = A.length;

        int gaussCompletion = 0;

        // create working copy of matrix, including solution column
        double[][] R = new double[n][n + 1];
        for (int i = 0; i < n; i++) {
            System.arraycopy(A[i], 0, R[i], 0, n);

            // A*p=0
            R[i][n] = 0;
        }

        for (int i = 0; i < n; i++, gaussCompletion++) {
            // sort rows, highest at topmost position
            double max = Math.abs(R[i][i]);
            int j = i;

            // find max
            for (int k = i + 1; k < n; k++) {
                if (Math.abs(R[k][i]) > max) {
                    max = Math.abs(R[k][i]);
                    j = k;
                }
            }

            // no pivot found, abort
            if (max < eps) {
                break;
            }

            // swap rows
            if (j != i) {
                double[] t = R[i];
                R[i] = R[j];
                R[j] = t;
            }

            // subtract from lower rows
            for (int k = i + 1; k < n; k++) {
                double fac = R[k][i] / R[i][i];

                // subtract from every cell of the row
                for (int l = i; l < n + 1; l++) {
                    R[k][l] -= fac * R[i][l];
                }
            }
        }

        if (gaussCompletion == n) {
            return new double[n];
        } else {
            double[][] T = new double[gaussCompletion][gaussCompletion];
            double[] v = new double[gaussCompletion];

            for (int i = 0; i < T.length; i++) {
                for (int j = i; j < T.length; j++) {
                    T[i][j] = R[i][j];
                }
                v[i] = -R[i][T.length];
            }
            // Tx = -v
            double[] x = backSubst(T, v);

            // p = (x,1,0...0)
            double[] p = new double[n];
            System.arraycopy(x, 0, p, 0, x.length);
            p[x.length] = 1;

            return p;
        }
    }

    /**
     * Diese Methode berechnet das Matrix-Vektor-Produkt A*x mit A einer nxm
     * Matrix und x einem Vektor der Laenge m. Sie eignet sich zum Testen der
     * Gauss-Loesung
     */
    public static double[] matrixVectorMult(double[][] A, double[] x) {
        int n = A.length;
        int m = x.length;

        double[] y = new double[n];

        for (int i = 0; i < n; i++) {
            y[i] = 0;
            for (int j = 0; j < m; j++) {
                y[i] += A[i][j] * x[j];
            }
        }

        return y;
    }
}

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

/*******************************************************************************
 * Copyright 2018 Maximilian Stark | Dakror <mail@dakror.de>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

/**
 * @author Maximilian Stark | Dakror
 */
public class UnitTests {
    @Test
    public void testSubst() {
        double[][] R = { { 5, 2, 3 }, { 0, 8, 2 }, { 0, 0, 10 } };
        double[] b = { 13, 2, 3 };

        double[] x = Gauss.backSubst(R, b);

        assertTrue(Util.vectorCompare(b, Gauss.matrixVectorMult(R, x)));
    }

    @Test
    public void testGauss() {
        double[][] R = { { 8, 2, 3 }, { 4, 5, 6 }, { 7, 8, 10 } };
        double[] b = { 6, 2, 3 };

        double[] x = Gauss.solve(R, b);
        double[] b1 = Gauss.matrixVectorMult(R, x);

        assertTrue(Util.vectorCompare(b, b1));
    }

    @Test
    public void testSing() {
        double[][] R = { { 4, 4, 4 }, { 4, 4, 4 }, { 4, 4, 4 } };

        double[] x = Gauss.solveSing(R);

        // TODO: i dunno how to test this
    }

    @Test
    public void testWikipedia() throws FileNotFoundException, IOException {
        LinkMatrix lm = new LinkMatrix();
        lm.read("bin/webseiten/lokal.txt");
        double rho = 0.15;
        double[] rank = PageRank.rank(lm.L, rho);
        Arrays.sort(rank);
        String r[] = PageRank.getSortedURLs(lm.urls, lm.L, rho);

        for (int i = 0; i < r.length && i < 20; i++) {
            System.out.print((int) (10000 * rank[rank.length - i - 1])
                    / 100.0 + "%\t");
            System.out.println(r[i]);
        }
    }
}

public class Test_Gleitpunktzahl {
    /**
     * Testklasse, ob alles funktioniert!
     */

    private static final int runs = 10;

    public static void main(String[] argv) {
        test_Gleitpunktzahl();
    }

    public static void test_Gleitpunktzahl() {

        /**********************************/
        /* Test der Klasse Gleitpunktzahl */
        /**********************************/

        System.out.println("-----------------------------------------");
        System.out.println("Test der Klasse Gleitpunktzahl");

        /*
         * Verglichen werden die BitFelder fuer Mantisse und Exponent und das
         * Vorzeichen
         */
        Gleitpunktzahl.setSizeMantisse(4);
        Gleitpunktzahl.setSizeExponent(2);

        Gleitpunktzahl x;
        Gleitpunktzahl y;
        Gleitpunktzahl gleitref = new Gleitpunktzahl();
        Gleitpunktzahl gleiterg;

        /* Test von setDouble */
        System.out.println("Test von setDouble");
        try {
            // Test: setDouble
            x = new Gleitpunktzahl(0.5);

            // Referenzwerte setzen
            gleitref = new Gleitpunktzahl(0.5);

            // Test, ob Ergebnis korrekt
            if (x.compareAbsTo(gleitref) != 0
                    || x.vorzeichen != gleitref.vorzeichen) {
                printErg("" + x.toDouble(), "" + gleitref.toDouble());
            } else {
                System.out.println("    Richtiges Ergebnis\n");
            }
        } catch (Exception e) {
            System.out.print("Exception bei der Auswertung des Ergebnis!!\n");
        }

        /* Addition */
        System.out.println("Test der Addition mit Gleitpunktzahl");
        try {
            // Test: Addition
            System.out.println("Test: Addition  x + y");
            x = new Gleitpunktzahl(3.25);
            y = new Gleitpunktzahl(0.5);

            // Referenzwerte setzen
            gleitref = new Gleitpunktzahl(3.25 + 0.5);

            // Berechnung
            gleiterg = x.add(y);

            // Test, ob Ergebnis korrekt
            if (gleiterg.compareAbsTo(gleitref) != 0
                    || gleiterg.vorzeichen != gleitref.vorzeichen) {
                printAdd(x.toString(), y.toString());
                printErg(gleiterg.toString(), gleitref.toString());
            } else {
                System.out.println("    Richtiges Ergebnis\n");
            }

            //////// Eigener Test ////////
            for (int i = 0; i < runs; i++) {
                double a = getRandom();
                double b = getRandom();
                Gleitpunktzahl ga = new Gleitpunktzahl(a);

                if (a > 7) ga.setInfinite(true);
                else if (a < -7) ga.setInfinite(false);

                Gleitpunktzahl gb = new Gleitpunktzahl(b);

                if (b > 7) gb.setInfinite(true);
                else if (b < -7) gb.setInfinite(false);

                Gleitpunktzahl gd = ga.add(gb);

                double c = ga.toDouble() + gb.toDouble();

                Gleitpunktzahl gc = new Gleitpunktzahl(c);

                if (gd.compareAbsTo(gc) != 0 || gd.vorzeichen != gc.vorzeichen) {
                    printAdd(ga.toString(), gb.toString());
                    printErg(gd.toString(), gc.toString());
                    break;
                }
            }

        } catch (Exception e) {
            System.out.print("Exception bei der Auswertung des Ergebnis!!\n");
        }

        /* Subtraktion */
        try {
            System.out.println("Test der Subtraktion mit Gleitpunktzahl");

            // Test: Addition
            System.out.println("Test: Subtraktion  x - y");
            x = new Gleitpunktzahl(3.25);
            y = new Gleitpunktzahl(2.75);

            // Referenzwerte setzen
            gleitref = new Gleitpunktzahl((3.25 - 2.75));

            // Berechnung
            gleiterg = x.sub(y);

            // Test, ob Ergebnis korrekt
            if (gleiterg.compareAbsTo(gleitref) != 0
                    || gleiterg.vorzeichen != gleitref.vorzeichen) {
                printSub(x.toString(), y.toString());
                printErg(gleiterg.toString(), gleitref.toString());
            } else {
                System.out.println("    Richtiges Ergebnis\n");
            }

            //////// Eigener Test ////////
            for (int i = 0; i < runs; i++) {
                double a = getRandom();
                double b = getRandom();
                Gleitpunktzahl ga = new Gleitpunktzahl(a);

                if (a > 7) ga.setInfinite(false);
                else if (a < -7) ga.setInfinite(true);
                else if (Math.abs(a) < 0.3) ga.setNull();

                Gleitpunktzahl gb = new Gleitpunktzahl(b);

                if (b > 7) gb.setInfinite(false);
                else if (b < -7) gb.setInfinite(true);
                else if (Math.abs(b) < 0.3) gb.setNull();

                Gleitpunktzahl gd = ga.sub(gb);

                double c = ga.toDouble() - gb.toDouble();

                Gleitpunktzahl gc = new Gleitpunktzahl(c);

                if (gd.compareAbsTo(gc) != 0 || gd.vorzeichen != gc.vorzeichen) {
                    printAdd(ga.toString(), gb.toString());
                    printErg(gd.toString(), gc.toString());
                    break;
                }
            }

        } catch (Exception e) {
            System.out.print("Exception bei der Auswertung des Ergebnis!!\n");
        }

        /* Sonderfaelle */
        System.out.println("Test der Sonderfaelle mit Gleitpunktzahl");

        try {
            // Test: Sonderfaelle
            // 0 - inf
            System.out.println("Test: Sonderfaelle");
            x = new Gleitpunktzahl(0.0);
            y = new Gleitpunktzahl(1.0 / 0.0);

            // Referenzwerte setzen
            gleitref.setInfinite(true);

            // Berechnung mit der Methode des Studenten durchfuehren
            gleiterg = x.sub(y);

            // Test, ob Ergebnis korrekt
            if (gleiterg.compareAbsTo(gleitref) != 0
                    || gleiterg.vorzeichen != gleitref.vorzeichen) {
                printSub(x.toString(), y.toString());
                printErg(gleiterg.toString(), gleitref.toString());
            } else {
                System.out.println("    Richtiges Ergebnis\n");
            }

            
            
            
            //////// Eigener Test ////////
            Gleitpunktzahl ga, gb, gc, gd;

            // 0 - inf 
            ga = new Gleitpunktzahl(0);
            gb = new Gleitpunktzahl(1.0 / 0.0);
            gc = new Gleitpunktzahl();
            gc.setInfinite(true);
            gd = ga.sub(gb);

            if (gd.compareAbsTo(gc) != 0 || gd.vorzeichen != gc.vorzeichen) {
                printAdd(ga.toString(), gb.toString());
                printErg(gd.toString(), gc.toString());
            }

            // 0 - -inf 
            ga = new Gleitpunktzahl(0);
            gb = new Gleitpunktzahl(-1.0 / 0.0);
            gc = new Gleitpunktzahl();
            gc.setInfinite(false);
            gd = ga.sub(gb);

            if (gd.compareAbsTo(gc) != 0 || gd.vorzeichen != gc.vorzeichen) {
                printAdd(ga.toString(), gb.toString());
                printErg(gd.toString(), gc.toString());
            }

            // inf - 0
            ga = new Gleitpunktzahl(1.0 / 0.0);
            gb = new Gleitpunktzahl(1.0 / 0.0);
            gc = new Gleitpunktzahl();
            gc.setNaN();
            gd = ga.sub(gb);

            if (gd.compareAbsTo(gc) != 0 || gd.vorzeichen != gc.vorzeichen) {
                printAdd(ga.toString(), gb.toString());
                printErg(gd.toString(), gc.toString());
            }

            // 0 + inf 
            ga = new Gleitpunktzahl(0);
            gb = new Gleitpunktzahl(1.0 / 0.0);
            gc = new Gleitpunktzahl();
            gc.setInfinite(false);
            gd = ga.add(gb);

            if (gd.compareAbsTo(gc) != 0 || gd.vorzeichen != gc.vorzeichen) {
                printAdd(ga.toString(), gb.toString());
                printErg(gd.toString(), gc.toString());
            }
        } catch (Exception e) {
            System.out
                    .print("Exception bei der Auswertung des Ergebnis in der Klasse Gleitpunktzahl!!\n");
        }

    }

    private static void printAdd(String x, String y) {
        System.out.println("    Fehler!\n      Es wurde gerechnet:            "
                + x + " + " + y);
    }

    private static void printSub(String x, String y) {
        System.out.println("    Fehler!\n      Es wurde gerechnet:            "
                + x + " - " + y + " = \n");
    }

    private static void printErg(String erg, String checkref) {
        System.out.println("      Ihr Ergebnis lautet:           " + erg
                + "\n      Das Korrekte Ergebnis lautet:  " + checkref + "\n");
    }

    private static double getRandom() {
        return (int) ((Math.random() - 0.5) * 20000) / 1000.0;
    }

}

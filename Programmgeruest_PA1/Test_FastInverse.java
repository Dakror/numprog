import javax.swing.JFrame;

/**
 * @author Christoph Riesinger (riesinge@in.tum.de)
 * @author Jürgen Bräckle (braeckle@in.tum.de)
 * @author Sebastian Rettenberger (rettenbs@in.tum.de)
 * @since November 02, 2012
 * @version 1.1
 * 
 *          This class just contains a main() method to use the FastMath class
 *          and to invoke the plotter.
 */
public class Test_FastInverse {

    /** Beispielwerte fuer IEEE Standard mit 32 Bits */
    private static int MAGIC_NUMBER = 0x5f34c48d;//0x5f3759df;

    private static int anzBitsExponent = 8;
    private static int anzBitsMantisse = 24;

    private static final double testNumber = 9.9541025;

    private static int magicNumberByBruteForce() {
        int iterations = 100000;
        double err[] = new double[iterations];
        int nums[] = new int[iterations];

        for (int i = 0; i < iterations; i++) {
            int m = MAGIC_NUMBER;
            if (i % 2 == 1) {
                m += i;
            } else {
                m -= i;
            }

            Gleitpunktzahl g = new Gleitpunktzahl(testNumber);
            FastMath.setMagic(m);
            double d = FastMath.absInvSqrtErr(g);
            //            System.out.println(g + " " + d + " 0x" + Integer.toHexString(m));
            err[i] = d;
            nums[i] = m;
        }
        int magic = 0;
        double m = Double.MAX_VALUE;
        for (int i = 0; i < iterations; i++) {
            if (err[i] < m) {
                m = err[i];
                magic = nums[i];
            }
        }

        return magic;
    }

    /**
     * Berechnet den höchsten absoluten Fehler anhand regelmäßiger Stichprobenrechnungen 
     * @param magic für diese gegebene Magic number
     * @param maxError ob der höchste oder der niedrigste Fehler zurückgegeben werden soll
     * @return den höchsten Fehler
     */
    private static double getError(int magic, boolean maxError) {
        FastMath.setMagic(magic);
        int runs = 1000;
        double step = 10. / runs;

        double error = 0;
        if (!maxError) error = Double.MAX_VALUE;

        for (int i = 1; i <= runs; i++) {
            Gleitpunktzahl g = new Gleitpunktzahl(step * i);

            double e = FastMath.absInvSqrtErr(g);
            if (maxError) {
                if (e > error) error = e;
            } else {
                if (e < error) error = e;
            }
        }

        return error;
    }

    /**
     * Approximiert die magic number rekursiv indem immer näher an ein besseres (auf den absoluten Fehler bezogen) und finales Ergebnis herangerückt wird
     * @param magic, letzte Magic number, zum Fortsetzen der Rekursion
     * @param err letzter Fehler, zum Vergleich
     * @return die beste gefundene Magic number
     */
    private static int magicNumberByRecursion(int magic, double err) {
        if (err == 0) {
            err = getError(magic, true);
        }

        // schaut nach links (auf dem Zahlenstrahl)
        for (int i = magic >> 1; i >= 1; i >>= 1) {
            int m = magic - i;
            if (m < 10) continue;
            double e = getError(m, true);
            if (e < err) {
                return magicNumberByRecursion(m, e);
            }
        }

        // schaut nach rechts (auf dem Zahlenstrahl)
        for (int i = magic >> 1; i >= 1; i >>= 1) {
            int m = magic + i;
            double e = getError(m, true);

            if (e < err) {
                return magicNumberByRecursion(m, e);
            }
        }

        return magic;
    }

    /**
     * Uses the FastMath class and invokes the plotter. In a logarithmically
     * scaled system, the exact solutions of 1/sqrt(x) are shown in green, and
     * the absolute errors of the Fast Inverse Square Root in red. Can be used
     * to test and debug the own implementation of the fast inverse square root
     * algorithm and to play while finding an optimal magic number.
     * 
     * @param args
     *            args is ignored.
     */
    public static void main(String[] args) {
        Gleitpunktzahl.setSizeExponent(anzBitsExponent);
        Gleitpunktzahl.setSizeMantisse(anzBitsMantisse);

        int magic = 0;

        magic = magicNumberByBruteForce();
        System.out.println("Brute force: 0x" + Integer.toHexString(magic) + ", min=" + getError(magic, false) + ", max=" + getError(magic, true));

        // This for loop can be used to find more similar or as good magic numbers
        //        for (int i = 0; i < 10; i++) {
        //            if(magic < 10) break;
        magic = magicNumberByRecursion(magic >> 1, 0);
        System.out.println("Recursively: 0x" + Integer.toHexString(magic) + ", min=" + getError(magic, false) + ", max=" + getError(magic, true));
        //        }
        System.out.println("============");

        System.out.println("Final magic number: 0x" + Integer.toHexString(magic));

        // There are several numbers with exact the same errors, e.g. for 8+4b: 
        /*
         *  Recursively: 0x17cdd534, min=1.277523355848187E-4, max=0.18986596360536456
            Recursively: 0x5f37534, min=1.277523355848187E-4, max=0.18986596360536456
            Recursively: 0x8db534, min=1.277523355848187E-4, max=0.18986596360536456
            Recursively: 0x1dd534, min=1.277523355848187E-4, max=0.18986596360536456
            Recursively: 0x77534, min=1.277523355848187E-4, max=0.18986596360536456
            Recursively: 0x1534, min=1.277523355848187E-4, max=0.18986596360536456
            Recursively: 0x534, min=1.277523355848187E-4, max=0.18986596360536456
         */

        // Best i've found:
        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
        // iEEE: 0x5f34c48d, min=3.4021026931174703E-6, max=0.17798628049464593 //
        // 8+4b: 0x371ff534, min=1.277523355848187E-4, max=0.18986596360536456  //
        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////

        FastMath.setMagic(magic);

        int numOfSamplingPts = 1001;
        float[] xData = new float[numOfSamplingPts];
        float[] yData = new float[numOfSamplingPts];
        float x = 0.10f;

        /* calculate data to plot */
        for (int i = 0; i < numOfSamplingPts; i++) {
            xData[i] = x;
            Gleitpunktzahl y = new Gleitpunktzahl(x);
            yData[i] = (float) FastMath.absInvSqrtErr(y);

            x *= Math.pow(100.0d, 1.0d / numOfSamplingPts);
        }

        /* initialize plotter */
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try {
            frame.add(new Plotter(xData, yData));
        } catch (InstantiationException exception) {
            exception.printStackTrace();
            System.exit(1);
        }
        frame.setSize(960, 720);
        frame.setLocation(0, 0);
        frame.setVisible(true);
    }
}

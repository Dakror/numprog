package dft;

import java.util.Arrays;

/**
 * Schnelle inverse Fourier-Transformation
 *
 * @author Sebastian Rettenberger
 */
public class IFFT {
    /**
     * Schnelle inverse Fourier-Transformation (IFFT).
     *
     * Die Funktion nimmt an, dass die Laenge des Arrays c immer eine
     * Zweierpotenz ist. Es gilt also: c.length == 2^m fuer ein beliebiges m.
     */
    public static Complex[] ifft(Complex[] c) {
        /*
        FUNCTION [v[0],...,v[n-1]]=IFFT(c[0],...,c[n-1],n)
        if n==1
            v[0] = c[0];
        else
            m = n/2;
            z1 = IFFT(c[0],c[2],...,c[n-2],n/2);
            z2 = IFFT(c[1],c[3],...,c[n-1],n/2);
            omega = exp(2*pi*i/n);
            for j=0,...,m-1
                v[j] = z1[j] + omega^j * z2[j];
                v[m+j] = z1[j] - omega^j * z2[j];
            end
        end
        return v;
         */

        if (c.length == 1) {
            return Arrays.copyOf(c, 1);
        }

        int m = c.length / 2;

        Complex[] z1 = new Complex[m];
        Complex[] z2 = new Complex[m];
        for (int i = 0; i < c.length; i++) {
            if (i % 2 == 0) z1[i / 2] = c[i];
            else z2[i / 2] = c[i];
        }

        z1 = ifft(z1);
        z2 = ifft(z2);

        Complex omega = Complex.fromPolar(1, 2 * Math.PI * 1 / c.length);

        Complex[] v = new Complex[c.length];
        for (int i = 0; i < m; i++) {
            v[i] = z1[i].add(omega.power(i).mul(z2[i]));
            v[m + i] = z1[i].sub(omega.power(i).mul(z2[i]));
        }
        return v;
    }
}

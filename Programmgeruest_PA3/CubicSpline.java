import java.util.Arrays;

/**
 * Die Klasse CubicSpline bietet eine Implementierung der kubischen Splines. Sie
 * dient uns zur effizienten Interpolation von aequidistanten Stuetzpunkten.
 *
 * @author braeckle
 *
 */
public class CubicSpline implements InterpolationMethod {

    /** linke und rechte Intervallgrenze x[0] bzw. x[n] */
    double a, b;

    /** Anzahl an Intervallen */
    int n;

    /** Intervallbreite */
    double h;

    /** Stuetzwerte an den aequidistanten Stuetzstellen */
    double[] y;

    /** zu berechnende Ableitunge an den Stuetzstellen */
    double yprime[];

    // Hermite-Basis
    final java.util.function.Function<Double, Double> H0 = t -> 1 - 3 * t * t + 2 * t * t * t;
    final java.util.function.Function<Double, Double> H1 = t -> 3 * t * t - 2 * t * t * t;
    final java.util.function.Function<Double, Double> H2 = t -> t - 2 * t * t + t * t * t;
    final java.util.function.Function<Double, Double> H3 = t -> -t * t + t * t * t;

    /**
     * {@inheritDoc} Zusaetzlich werden die Ableitungen der stueckweisen
     * Polynome an den Stuetzstellen berechnet. Als Randbedingungen setzten wir
     * die Ableitungen an den Stellen x[0] und x[n] = 0.
     */
    @Override
    public void init(double a, double b, int n, double[] y) {
        this.a = a;
        this.b = b;
        this.n = n;
        h = (b - a) / (n);

        this.y = Arrays.copyOf(y, n + 1);

        /* Randbedingungen setzten */
        yprime = new double[n + 1];
        yprime[0] = 0;
        yprime[n] = 0;

        /* Ableitungen berechnen. Nur noetig, wenn n > 1 */
        if (n > 1) {
            computeDerivatives();
        }
    }

    /**
     * getDerivatives gibt die Ableitungen yprime zurueck
     */
    public double[] getDerivatives() {
        return yprime;
    }

    /**
     * Setzt die Ableitungen an den Raendern x[0] und x[n] neu auf yprime0 bzw.
     * yprimen. Anschliessend werden alle Ableitungen aktualisiert.
     */
    public void setBoundaryConditions(double yprime0, double yprimen) {
        yprime[0] = yprime0;
        yprime[n] = yprimen;
        if (n > 1) {
            computeDerivatives();
        }
    }

    /**
     * Berechnet die Ableitungen der stueckweisen kubischen Polynome an den
     * einzelnen Stuetzstellen. Dazu wird ein lineares System Ax=c mit einer
     * Tridiagonalen Matrix A und der rechten Seite c aufgebaut und geloest.
     * Anschliessend sind die berechneten Ableitungen y1' bis yn-1' in der
     * Membervariable yprime gespeichert.
     *
     * Zum Zeitpunkt des Aufrufs stehen die Randbedingungen in yprime[0] und yprime[n].
     * Speziell bei den "kleinen" Faellen mit Intervallzahlen n = 2
     * oder 3 muss auf die Struktur des Gleichungssystems geachtet werden. Der
     * Fall n = 1 wird hier nicht beachtet, da dann keine weiteren Ableitungen
     * berechnet werden muessen.
     */
    public void computeDerivatives() {
        TridiagonalMatrix A = new TridiagonalMatrix(n - 1);
        for (int i = 1; i <= n - 1; i++) {
            A.setElement(i, i, 4);
            if (i < n - 1) A.setElement(i + 1, i, 1);
            if (i > 1) A.setElement(i - 1, i, 1);
        }

        double scale = 3 / h;
        double[] b = new double[n - 1];
        b[0] = scale * (y[2] - y[0] - h / 3 * yprime[0]);
        b[n - 2] = scale * (y[n] - y[n - 2] - h / 3 * yprime[n]);

        for (int i = 1; i < b.length - 1; i++) {
            b[i] = scale * (y[i + 2] - y[i]);
        }

        double[] x = A.solveLinearSystem(b);
        System.arraycopy(x, 0, yprime, 1, x.length);
    }

    /**
     * {@inheritDoc} Liegt z ausserhalb der Stuetzgrenzen, werden die
     * aeussersten Werte y[0] bzw. y[n] zurueckgegeben. Liegt z zwischen den
     * Stuetzstellen x_i und x_i+1, wird z in das Intervall [0,1] transformiert
     * und das entsprechende kubische Hermite-Polynom ausgewertet.
     */
    @Override
    public double evaluate(double z) {
        if (z < a) return y[0];
        else if (z > b) return y[n];

        // Finde das Intervall I = [xi, xi+1] mit x in I und damit das entsprechende Polynom
        for (int i = 0; i < (b - a) / h; i++) {
            if (z >= a + i * h && z <= a + i * h + h) {
                // Transformiere x in das Intervall [0, 1] mit Hilfe von:
                double t = (z - (a + i * h)) / h;

                // Werte nun das Polynom q zur Hermite-Basis in t aus:
                return y[i] * H0.apply(t) + y[i + 1] * H1.apply(t) + h * yprime[i] * H2.apply(t) + h * yprime[i + 1] * H3.apply(t);
            }
        }

        // distinct error value
        return -1.337;
    }
}

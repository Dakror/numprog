import java.util.Arrays;
import java.util.stream.Collectors;

import dft.Complex;

public class Test {

    /**
     * @param args
     */
    public static void main(String[] args) {
        testNewton();
        testSplines();
        testFFT();
        testFFT2();
    }

    private static void testNewton() {
        NewtonPolynom p;
        double[] x, y;

        x = new double[] { -1.5, -0.75, 0, 0.75, 1.5 };
        y = new double[x.length];
        for (int i = 0; i < x.length; i++)
            y[i] = Math.tan(x[i]);

        p = new NewtonPolynom(x, y);
        System.out.println("a: " + Arrays.toString(p.getCoefficients()) + ", f: " + Arrays.toString(p.getDividedDifferences()));

        x = new double[] { 0, 1, 2/*, 1.5 */ };
        y = new double[] { 3, 0, 1, 0 };
        p = new NewtonPolynom(x, y);

        System.out.println("a: " + Arrays.toString(p.getCoefficients()) + ", f: " + Arrays.toString(p.getDividedDifferences()));

        x = new double[] { -1, 1, 3 };
        y = new double[] { -3, 1, -3 };
        p = new NewtonPolynom(x, y);

        System.out.println(Arrays.toString(p.getDividedDifferences()));

        System.out.println(p.evaluate(0) + " sollte sein: 0.0");
        System.out.println("-------------------------------");
    }

    public static void testSplines() {
        CubicSpline spl = new CubicSpline();
        double[] y = { 2, 0, 2, 3 };
        spl.init(-1, 2, 3, y);
        spl.setBoundaryConditions(9, 0);
        System.out.println(Arrays.toString(spl.getDerivatives())
                + " sollte sein: [9.0, -3.0, 3.0, 0.0].");
    }

    public static void testFFT() {
        System.out.println("Teste Fast Fourier Transformation");

        double[] v = new double[4];
        for (int i = 0; i < 4; i++)
            v[i] = i + 1;
        Complex[] c = dft.DFT.dft(v);
        Complex[] v2 = dft.IFFT.ifft(c);

        for (int i = 0; i < 4; i++) {
            System.out.println(v2[i]);
        }
        System.out.println("Richtig waeren gerundet: Eigene Beispiele ueberlegen");

        System.out.println("*************************************\n");
    }

    public static void testFFT2() {
        System.out.println("testing IFFT(DFT(c))=c");
        double[] c = { 1, 2, 3, 4, 5, 6, 7, 8 };

        System.out.println("started with: " + Arrays.toString(c));
        System.out.println("resulted in:  [" + Arrays.stream(dft.IFFT.ifft(dft.DFT.dft(c))).sequential().map(x -> Double.toString(Math.round(x.getRadius()))).collect(Collectors.joining(", ")) + "]");
    }
}
